package ru.svs.fraction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Класс для демонстрации работы класса Rational.
 *
 * @author Щеголева В., группа 15/18
 */

public class Demo {

    public static void main(String[] args) throws IOException {
        FileWriter fileWriter = new FileWriter("input.txt");
        BufferedReader bufferedReader = new BufferedReader(new FileReader("output.txt"));
        String str = bufferedReader.readLine();
        String[] arr = str.split(" ");
        String one = arr[0];
        String two = arr[2];
        String[] array = one.split("/");
        int numArr[] = new int[array.length];
        for (int j = 0; j < array.length; j++) {
            numArr[j] = Integer.parseInt(array[j]);
        }
        int num = numArr[0];
        int denum = numArr[1];


        String[] arraytwo = two.split("/");
        int numArrTwo[] = new int[arraytwo.length];
        for (int j = 0; j < arraytwo.length; j++) {
            numArrTwo[j] = Integer.parseInt(arraytwo[j]);
        }

        int numtwo = numArrTwo[0];
        int denumtwo = numArrTwo[1];

        Rational rational1 = new Rational(numtwo, denumtwo);
        Rational rational = new Rational(num, denum);


        if (arr[1].equals("+")) {
            fileWriter.write(String.valueOf(rational.add(rational1)));
        }
        if (arr[1].equals("-")) {
            fileWriter.write(String.valueOf(rational.subtract(rational1)));
        }
        if (arr[1].equals("*")) {
            fileWriter.write(String.valueOf(rational.multiply(rational1)));
        }
        if (arr[1].equals(":")) {
            fileWriter.write(String.valueOf(rational.div(rational1)));
        }

        fileWriter.close();
        bufferedReader.close();

    }

}
